package com.devcamp_province.provincedistrictward.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp_province.provincedistrictward.model.*;
import com.devcamp_province.provincedistrictward.repository.*;

@Service
public class DistrictService {
    @Autowired
    IProvinceRepository iProvinceRepository;
    @Autowired
    IDistrictRepository iDistrictRepository;
    
    public List<CDistrict> getAllDistricts() {
        List<CDistrict> listDistrict = new ArrayList<CDistrict>();
        iDistrictRepository.findAll().forEach(listDistrict::add);
        return listDistrict;
    }

    public List<CDistrict> getDistrictsByProvinceId(int provinceId) {
        Optional<CProvince> existedProvince = iProvinceRepository.findById(provinceId);
        if(existedProvince.isPresent()) {
            List<CDistrict> listDistrict = new ArrayList<CDistrict>();
            existedProvince.get().getDistricts().forEach(listDistrict::add);
            return listDistrict;
        } else {
            return null;
        }
    }

    public CDistrict getDistrictById(int id) {
        Optional<CDistrict> existedDistrict = iDistrictRepository.findById(id);
        if(existedDistrict.isPresent()) {
            return existedDistrict.get();
        } else {
            return null;
        }
    }

    public CDistrict createDistrict(int provinceId, CDistrict pDistrict) {
        Optional<CProvince> existedProvince = iProvinceRepository.findById(provinceId);
        if(existedProvince.isPresent()) {
            CDistrict newDistrict = new CDistrict();
            newDistrict.setPrefix(pDistrict.getPrefix());
            newDistrict.setName(pDistrict.getName());
            newDistrict.setProvince(existedProvince.get());
            return newDistrict;
        } else {
            return null;
        }
    }

    public CDistrict updateDistrictById(int id, CDistrict pDistrict) {
        Optional<CDistrict> existedDistrict = iDistrictRepository.findById(id);
        if(existedDistrict.isPresent()) {
            CDistrict updatedDistrict = existedDistrict.get();
            updatedDistrict.setPrefix(pDistrict.getPrefix());
            updatedDistrict.setName(pDistrict.getName());
            return updatedDistrict;
        } else {
            return null;
        }
    }
}
