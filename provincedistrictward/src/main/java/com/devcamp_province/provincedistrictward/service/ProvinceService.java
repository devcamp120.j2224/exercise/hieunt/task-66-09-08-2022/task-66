package com.devcamp_province.provincedistrictward.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp_province.provincedistrictward.model.CProvince;
import com.devcamp_province.provincedistrictward.repository.IProvinceRepository;

@Service
public class ProvinceService {
    @Autowired
    IProvinceRepository iProvinceRepository;

    public List<CProvince> getAllProvinces() {
        List<CProvince> listProvince = new ArrayList<CProvince>();
        iProvinceRepository.findAll().forEach(listProvince::add);
        return listProvince;
    }

    public CProvince getProvinceById(int id) {
        Optional<CProvince> existedProvince = iProvinceRepository.findById(id);
        if(existedProvince.isPresent()) {
            return existedProvince.get();
        } else {
            return null;
        }
    }

    public CProvince createProvince(CProvince pProvince) {
        CProvince newProvince = new CProvince();
        newProvince.setCode(pProvince.getCode());
        newProvince.setName(pProvince.getName());
        return newProvince;
    }

    public CProvince updateProvinceById(int id, CProvince pProvince) {
        Optional<CProvince> existedProvince = iProvinceRepository.findById(id);
        if(existedProvince.isPresent()) {
            CProvince updatedProvince = existedProvince.get();
            updatedProvince.setCode(pProvince.getCode());
            updatedProvince.setName(pProvince.getName());
            return updatedProvince;
        } else {
            return null;
        }
    }
}
