package com.devcamp_province.provincedistrictward.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp_province.provincedistrictward.model.*;
import com.devcamp_province.provincedistrictward.repository.*;
import com.devcamp_province.provincedistrictward.service.*;

@RequestMapping("/ward")
@RestController
@CrossOrigin(value = "*" , maxAge = -1)
public class WardController {
    @Autowired
    IWardRepository iWardRepository;
    @Autowired
    WardService WardService;

    @GetMapping("/all")
    public ResponseEntity<Object> getAllWards() {
        List<CWard> listWard = WardService.getAllWards();
        try {
            return new ResponseEntity<>(listWard, HttpStatus.OK);
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all/districtId/{districtId}")
    public ResponseEntity<Object> getWardsByDistrictId(@PathVariable int districtId) {
        List<CWard> listWard = WardService.getWardsByDistrictId(districtId);
        if(listWard != null) {
            try {
                return new ResponseEntity<>(listWard, HttpStatus.OK);
            } catch(Exception ex) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("District not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getWardById(@PathVariable int id) {
        CWard existedWard = WardService.getWardById(id);
        if(existedWard != null) {
            try {
                return new ResponseEntity<>(existedWard, HttpStatus.OK);
            } catch(Exception ex) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("Ward not found", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createWard(@RequestParam int districtId, @RequestBody CWard pWard) {
        CWard newWard = WardService.createWard(districtId, pWard);
        if(newWard != null) {
            try {
                return new ResponseEntity<>(iWardRepository.save(newWard), HttpStatus.CREATED);
            } catch(Exception ex) {
                return new ResponseEntity<>("Failed to Create Ward", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("District not found", HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateWardById(@PathVariable int id, @RequestBody CWard pWard) {
        CWard updatedWard = WardService.updateWardById(id, pWard);
        if(updatedWard != null) {
            try {
                return new ResponseEntity<>(iWardRepository.save(updatedWard), HttpStatus.OK);
            } catch(Exception ex) {
                return new ResponseEntity<>("Failed to Update Ward", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("Ward not found", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<Object> deleteWardById(@PathVariable int id) {
        Optional<CWard> existedWard = iWardRepository.findById(id);
        if(existedWard != null) {
            try {
                iWardRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } catch(Exception ex) {
                return new ResponseEntity<>("Failed to Delete Ward", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("Ward not found", HttpStatus.NOT_FOUND);
        }
    }
}
